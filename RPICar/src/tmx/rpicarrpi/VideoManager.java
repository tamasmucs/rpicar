/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tmx.rpicarrpi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UTFDataFormatException;
import java.util.logging.Level;
import java.util.logging.Logger;
import tmx.rpicarcommon.CarComponent;

/**
 *
 * @author tamas
 */
public class VideoManager extends CarComponent {

    private static final int PORT = 2177;
    private static final String PIC_PATH = "/home/pi/currentSnapshot.jpg";
    File picFile;
    //private FileReader reader;
    private FileInputStream reader;
    private byte imageData[];

    public VideoManager() throws FileNotFoundException, IOException {
        super(PORT, true);
        picFile = new File(PIC_PATH);

    }

    @Override
    public void run() {
        super.connect();
        if (RPICar.isRPi()) {
            while (true) {
                try {
                    //capture an image and save it into PIC_PATH
                    Runtime rt = Runtime.getRuntime();
                    String[] cmd = {
                        "/bin/sh",
                        "-c",
                        "raspistill -w 150 -h 150 -o currentSnapshot.jpg -t 1 --nopreview"
                    };
                    Process proc = rt.exec(cmd);
                    proc.waitFor();
                    //send the pic file
                    reader = new FileInputStream(picFile);
                    imageData = new byte[(int) picFile.length()];
                    reader.read(imageData);
                    String imageDataString = Base64.encodeBytes(imageData);
                    try {
                        out.writeUTF(imageDataString);
                    } catch (UTFDataFormatException uTFDataFormatException) {
                        System.out.println("image was too big to be sent");
                    }
                    out.flush();
                } catch (FileNotFoundException fnfex) {
                    System.out.println("File Not found @ sending picture.");
                } catch (IOException | InterruptedException ex) {
                    Logger.getLogger(VideoManager.class.getName()).log(Level.SEVERE, null, ex);
                }                
            }
        }
    }
}
