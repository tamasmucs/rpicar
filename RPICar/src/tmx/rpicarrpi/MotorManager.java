package tmx.rpicarrpi;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import tmx.rpicarcommon.Direction;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import tmx.rpicarcommon.CarComponent;
import java.net.InetAddress;

public class MotorManager extends CarComponent {

    private Direction currentDirection;

    private static final int PORT = 2175;
    private GpioController gpio;

    private GpioPinDigitalOutput pinRWD;
    private GpioPinDigitalOutput pinFWD;
    private GpioPinDigitalOutput pinL;
    private GpioPinDigitalOutput pinR;

    public MotorManager() {
        super(PORT, true);
        //assign pins if not on pc
        if (RPICar.isRPi()) {
            gpio = GpioFactory.getInstance();
            pinRWD = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_22, PinState.LOW);
            pinFWD = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_23, PinState.LOW);
            pinL = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_24, PinState.LOW);
            pinR = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_25, PinState.LOW);
        }   
        currentDirection = new Direction();
    }

    @Override
    public void run() {
        super.connect();
        while (true) {
            try {
                currentDirection = (Direction) super.in.readObject();
                if (!RPICar.isRPi()) {
                    System.out.println(currentDirection);
                } else {
                    //System.out.println(currentDirection.getV()+" "+currentDirection.getH());

                    switch (currentDirection.getV()) {
                        case -1:
                            pinRWD.high();
                            //pinFWD.low();
                            break;
                        case 0:
                            //pinRWD.low();
                            //pinFWD.low();
                            break;
                        case 1:
                            pinFWD.high();
                            //pinRWD.low();
                            break;
                    }
                    switch (currentDirection.getH()) {
                        case -1:
                            //pinR.low();
                            pinL.high();
                            break;
                        case 0:
                            //pinR.low();
                            //pinL.low();
                            break;
                        case 1:
                            //pinL.low();
                            pinR.high();
                            break;
                    }
                    Thread.sleep(500);
                    resetPins();
                }
            } catch (IOException ex) {
                currentDirection.reset();
                if (RPICar.isRPi()) {
                    this.resetPins();
                }
                super.connect();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(RPICar.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(MotorManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void resetPins() {
        this.pinFWD.low();
        this.pinRWD.low();
        this.pinL.low();
        this.pinR.low();
    }
}
