/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tmx.rpicarrpi;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

/**
 *
 * @author tamas
 */
public class Test {
    public static void led25Blink() throws InterruptedException{
        final GpioController gpio = GpioFactory.getInstance();
        // provision gpio pin #01 as an output pin and turn on
        final GpioPinDigitalOutput pin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_25, PinState.HIGH);
        while(true){
            pin.setShutdownOptions(true, PinState.LOW);

            System.out.println("--> GPIO state should be: ON");

            Thread.sleep(Integer.MAX_VALUE);

            // turn off gpio pin #01
            pin.low();
            System.out.println("--> GPIO state should be: OFF");

            Thread.sleep(5000);

            // toggle the current state of gpio pin #01 (should turn on)
            pin.toggle();
            System.out.println("--> GPIO state should be: ON");

            Thread.sleep(5000);

            // toggle the current state of gpio pin #01  (should turn off)
            pin.toggle();
            System.out.println("--> GPIO state should be: OFF");
            Thread.sleep(5000);
            
        }
    }
    public static void ledTest() throws InterruptedException{
            GpioController gpio = GpioFactory.getInstance();
            
            GpioPinDigitalOutput pinRWD;
            GpioPinDigitalOutput pinFWD;
            GpioPinDigitalOutput pinL;
            GpioPinDigitalOutput pinR;
            
            pinRWD = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_22, PinState.LOW);
            pinFWD = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_23, PinState.LOW);
            pinL = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_24, PinState.LOW);
            pinR = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_25, PinState.LOW);
            
            while(true){
                pinRWD.high();
                Thread.sleep(1000);
                pinRWD.low();
                pinFWD.high();
                Thread.sleep(1000);
                pinFWD.low();
                pinL.high();
                Thread.sleep(1000);
                pinL.low();
                pinR.high();
                Thread.sleep(1000);
                pinR.low();
            }
    }
}
