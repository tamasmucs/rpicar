package tmx.rpicarrpi;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;

public class RPICar {

    public static final boolean isRPi(){
        return System.getProperty("os.name").equals("Linux");
    }
    
    public static void main(String[] args) throws IOException, InterruptedException {
        normalStart();
    }
    
    private static void normalStart() throws FileNotFoundException, IOException{
        MotorManager mManager = new MotorManager();
        Thread mThread = new Thread (mManager);
        mThread.start();
        
        EnvironmentManager eManager = new EnvironmentManager();
        Thread eThread = new Thread(eManager);
        eThread.start();
        
        VideoManager vManager = new VideoManager();
        Thread vThread = new Thread (vManager);
        vThread.start();
    }
    
}