package tmx.rpicarrpi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.DoubleStream;
import tmx.rpicarcommon.CarComponent;
import tmx.rpicarcommon.EnvironmentData;
import java.net.InetAddress;

public class EnvironmentManager extends CarComponent {
    
    EnvironmentData currentEnvironemntData;
    private static final int PORT = 2176;
    public void setCurrentEnvironemntData(EnvironmentData currentEnvironemntData) {
        this.currentEnvironemntData = currentEnvironemntData;
    }

    public EnvironmentData getCurrentEnvironemntData() {
        return currentEnvironemntData;
    }
    public EnvironmentManager() {
        super(PORT, true);
        this.currentEnvironemntData = new EnvironmentData();
    }

    @Override
    public void run() {
        super.connect();
        while(true){
            try {
                measure();
            } catch (IOException ex) {
                System.out.println("IO Excepton @ Measuring: "+ex.getMessage());
            }
            try {
                super.out.writeObject(this.getCurrentEnvironemntData());
            } catch (IOException ex) {
                System.out.println("Lost connection, eManager reconnecting on car");
                super.connect();
            }
        }
    }

    private synchronized void measure() throws IOException {
        if(RPICar.isRPi()){
            
            String resultMsg;
            //measure temperature
            double temperature = 0;
            resultMsg = executeShellCommand("cat /sys/bus/w1/devices/28-00000474cd5f/w1_slave | grep -o [0-9][0-9][0-9][0-9][0-9]");
            if(resultMsg != null){
                temperature = Double.parseDouble(resultMsg) / 1000f;
            }
            //Measure distance 
            double distance = 0;
            resultMsg = executeShellCommand("python /home/pi/MeasureDistance.py");
            if(resultMsg != null){
                distance = Double.parseDouble(resultMsg);
            }
            this.setCurrentEnvironemntData(this.currentEnvironemntData = new EnvironmentData(temperature, distance));
        }else{
            EnvironmentData ed = new EnvironmentData(Math.random(), Math.random());
            this.setCurrentEnvironemntData(ed);
        }
    }
    
    private static String executeShellCommand(String cmd) throws IOException{
        Runtime rt = Runtime.getRuntime();
        String[] cmdArray = {
            "/bin/sh",
            "-c",
            cmd
        };
        Process proc = rt.exec(cmdArray);
        BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        String resultMsg = stdInput.readLine();
        return resultMsg;
    }
}